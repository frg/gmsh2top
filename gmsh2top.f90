PROGRAM conv_geom

  IMPLICIT NONE

  INTEGER :: N_phys, N_nodes, N_ele, N_Dele, N_BCele, N_surf, N_vol

  CHARACTER(len=64)  :: in_file, out_file, line
  CHARACTER(len=258) :: str

  TYPE type_bc
     CHARACTER(len=120) :: bc_name
     INTEGER :: bc_id
  END TYPE type_bc
  TYPE(type_bc), DIMENSION(:), ALLOCATABLE :: list_bc

  TYPE type_ele
    INTEGER, DIMENSION(:), ALLOCATABLE :: nodes
    INTEGER :: bc
  END TYPE type_ele
  TYPE(type_ele), DIMENSION(:), ALLOCATABLE :: ele

  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: coord

  INTEGER, DIMENSION(:), ALLOCATABLE :: v_dummy
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: v_dummyr

  INTEGER :: i, j, k, n, et, n_bc, ierror, dummy
  REAL(KIND=8) :: x, y, z, vers

  INTEGER, DIMENSION(4) :: ent
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: surf_map, vol_map
  INTEGER, DIMENSION(:), ALLOCATABLE :: node_map_tmp, node_map

  !--------------------------------------

  IF(command_argument_count() < 1) THEN
     WRITE(*,*) 'ERROR: No mesh file'
     WRITE(*,*) 'STOP!'
     STOP
  ENDIF
  CALL get_command_argument(1, in_file)
  OPEN(10, file=TRIM(ADJUSTL(in_file))//".msh", ACTION = 'READ', IOSTAT = ierror)
  IF(ierror /= 0) THEN
     WRITE(*,*) 'ERROR: cannot open the input file', TRIM(ADJUSTL(in_file))//".msh"
     STOP
  ENDIF
  out_file = TRIM(ADJUSTL(in_file)) // ".top"
  OPEN(15, FILE = TRIM(ADJUSTL(out_file)), ACTION = 'WRITE', IOSTAT = ierror)
  IF(ierror /= 0) THEN
     WRITE(*,*) 'ERROR: cannnot open the output file'
     STOP
  ENDIF

  !----------------
  ! Read input mesh format
  READ(10, *) line
  DO WHILE(line .NE. '$MeshFormat')
    READ(10, *) line
  ENDDO
  READ(10, *) vers
  WRITE(*,"(A,F3.1)") 'Reading MSH format version = ', vers
  !----------------

  !----------------
  ! Physical Names
  READ(10, *) line
  DO WHILE(line .NE. '$PhysicalNames')
    READ(10, *) line
  ENDDO
  READ(10,*) N_phys
  ALLOCATE(list_bc(N_phys))
  DO i = 1, N_phys
     READ(10,*) dummy, list_bc(i)%bc_id, list_bc(i)%bc_name
     list_bc(i)%bc_name = TRIM(ADJUSTL(list_bc(i)%bc_name))
  ENDDO
  !----------------

  !----------------
  ! Surface and Volume Entities (version 4 only)
  IF(vers .GE. 4.0) THEN
    READ(10, *) line
    DO WHILE(line .NE. '$Entities')
      READ(10, *) line
    ENDDO
    READ(10,*) ent
    ! Points
    DO i = 1, ent(1)
      READ(10, *)
    ENDDO
    ! Lines
    DO i = 1, ent(2)
      READ(10, *)
    ENDDO
    ! Surfaces
    ALLOCATE(v_dummyr(6))
    N_surf = ent(3)
    ALLOCATE(surf_map(2,N_surf))
    DO i = 1, N_surf
      READ(10, *) surf_map(1,i), v_dummyr, dummy, surf_map(2,i)
    ENDDO
    ! Volumes
    N_vol = ent(4)
    ALLOCATE(vol_map(2,N_vol))
    DO i = 1, N_vol
      READ(10, *) vol_map(1,i), v_dummyr, dummy, vol_map(2,i)
    ENDDO
    DEALLOCATE(v_dummyr)
  ENDIF
  !----------------

  !----------------
  ! Nodes
  READ(10, *) line
  DO WHILE(line .NE. '$Nodes')
    READ(10, *) line
  ENDDO
  IF(vers .GE. 4.0) THEN
    READ(10, *) dummy, N_nodes
  ELSE
    READ(10, *) N_nodes
  ENDIF

  WRITE(15, '("Nodes FluidNodes")')
  ALLOCATE(coord(3, N_nodes))
  IF(vers .GE. 4.0) THEN
    ALLOCATE(node_map_tmp(N_nodes))
    i = 0; n = -1
    DO WHILE(i < N_nodes)
      READ(10, *) ent
      DO k = 1, ent(4)
        i = i + 1
        READ(10, *) node_map_tmp(i), coord(:,i)
        IF(node_map_tmp(i) .GT. n) THEN
          n = node_map_tmp(i)
        ENDIF
        WRITE(15, '(I9, 3E24.16)') i, coord(:,i)
      ENDDO
    ENDDO

    ALLOCATE(node_map(n))
    DO i = 1, N_nodes
      node_map(node_map_tmp(i)) = i
    ENDDO
    DEALLOCATE(node_map_tmp)
  ELSE
    DO i = 1, N_nodes
       READ(10, *) dummy, coord(:,i)
       WRITE(15, '(I9, 3E24.16)') i, coord(:,i)
    ENDDO
  ENDIF
  !----------------

  !----------------
  ! Elements
  READ(10, *) line
  DO WHILE(line .NE. '$Elements')
    READ(10, *) line
  ENDDO
  IF(vers .GE. 4.0) THEN
    READ(10, *) dummy, N_ele
  ELSE
    READ(10, *) N_ele
  ENDIF

  N_Dele = 0; N_BCele = 0
  ALLOCATE(ele(N_ele))
  IF(vers .GE. 4.0) THEN
    i = 0
    DO WHILE(i < N_ele)
      READ(10, *) ent
      IF(ent(3) == 2) THEN
        DO j = 1, N_surf
          IF(ent(1) == surf_map(1,j)) THEN
            n = surf_map(2,j)
            EXIT
          ENDIF
        ENDDO

        DO j = 1, ent(4)
          N_BCele = N_BCele + 1
          i = i + 1
          ALLOCATE(ele(i)%nodes(3))
          READ(10, *) dummy, ele(i)%nodes
          ele(i)%bc = n
          ! Recenter node numbers
          DO k = 1, 3
            ele(i)%nodes(k) = node_map(ele(i)%nodes(k))
          ENDDO
        ENDDO
      ELSEIF(ent(3) == 4) THEN
        DO j = 1, N_vol
          IF(ent(1) == vol_map(1,j)) THEN
            n = vol_map(2,j)
            EXIT
          ENDIF
        ENDDO

        DO j = 1, ent(4)
          N_Dele = N_Dele + 1
          i = i + 1
          ALLOCATE(ele(i)%nodes(4))
          READ(10, *) dummy, ele(i)%nodes
          ele(i)%bc = n
          ! Recenter node numbers
          DO k = 1, 4
            ele(i)%nodes(k) = node_map(ele(i)%nodes(k))
          ENDDO
        ENDDO
      ELSE
        WRITE(*,*) 'GO HOME: BAD ELEMENT TYPE'
        STOP
      ENDIF
    ENDDO
    DEALLOCATE(node_map)
  ELSE
    DO i = 1, N_ele
      READ(10, *) dummy, et, n
      IF(et == 2) THEN
        ALLOCATE(ele(i)%nodes(3))
        N_BCele = N_BCele + 1
      ELSEIF(et == 4) THEN
        ALLOCATE(ele(i)%nodes(4))
        N_Dele = N_Dele + 1
      ELSE
        WRITE(*,*) 'GO HOME: BAD ELEMENT TYPE'
        STOP
      ENDIF
      ALLOCATE(v_dummy(n))
      BACKSPACE(10)
      READ(10, *) dummy, dummy, dummy, v_dummy, ele(i)%nodes
      ele(i)%bc = v_dummy(1)
      DEALLOCATE(v_dummy)
    ENDDO
  ENDIF
  !----------------

  CLOSE(10)

  !----------------
  ! Write Elements
  n = 0
  WRITE(*,*) N_Dele, N_BCele
  IF(N_Dele > 0) THEN
     DO k = 1, SIZE(list_bc)
        n_bc = 0
        str = 'Elements ' // TRIM(ADJUSTL(list_bc(k)%bc_name)) // ' using FluidNodes'
        DO i = 1, N_ele
           IF(SIZE(ele(i)%nodes) == 4 .AND. ele(i)%bc == list_bc(k)%bc_id) THEN
              IF(n_bc == 0) THEN
                WRITE(15, '(a)') TRIM(ADJUSTL(str))
              ENDIF
              n = n + 1
              WRITE(15,'(I9, I3, 4I9)') n, 5, ele(i)%nodes
              n_bc = n_bc + 1
           ENDIF
        ENDDO
        IF(n_bc > 0) WRITE(*,*) TRIM(ADJUSTL(str)), n_bc
     ENDDO
  ENDIF
  IF(N_BCele > 0) THEN
     DO k = 1, SIZE(list_bc)
        n_bc = 0
        str = 'Elements ' // TRIM(ADJUSTL(list_bc(k)%bc_name)) // ' using FluidNodes'
        DO i = 1, N_ele
           IF(SIZE(ele(i)%nodes) == 3 .AND. ele(i)%bc == list_bc(k)%bc_id) THEN
              IF(n_bc == 0) THEN
                WRITE(15,'(a)') TRIM(ADJUSTL(str))
              ENDIF
              n = n + 1
              WRITE(15,'(I9, I3, 3I9)') n, 4, ele(i)%nodes
              n_bc = n_bc + 1
           ENDIF
        ENDDO
        IF(n_bc > 0) WRITE(*,*) TRIM(ADJUSTL(str)), n_bc
     ENDDO
  ENDIF
  !----------------

  CLOSE(15)

CONTAINS

   FUNCTION tet_vol(xx) RESULT(vol)

    IMPLICIT NONE

    REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: xx
    REAL(KIND=8) :: vol

    REAL(KIND=8), DIMENSION(3) :: a_d, b_d, c_d, bd_x_cd

    a_d = xx(:, 1) - xx(:, 4)
    b_d = xx(:, 2) - xx(:, 4)
    c_d = xx(:, 3) - xx(:, 4)

    bd_x_cd(1) = b_d(2)*c_d(3) - b_d(3)*c_d(2)
    bd_x_cd(2) = b_d(3)*c_d(1) - b_d(1)*c_d(3)
    bd_x_cd(3) = b_d(1)*c_d(2) - b_d(2)*c_d(1)

    vol = -DOT_PRODUCT(a_d, bd_x_cd)/6.d0;

  END FUNCTION tet_vol

END PROGRAM conv_geom
